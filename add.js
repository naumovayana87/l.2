document.addEventListener("DOMContentLoaded", function () {
    (async function () {
        let data=new Date().getDay();
        let year=new Date().getFullYear();
        let month=new Date().getMonth();
        if (month<=9||data<=9){
            month='0'+month;
            data='0'+data;
        }else{
            month;
            data;
        }
       let fullData=year+month+data;
        let url = `https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?date=${fullData}&json`;

        let result = await fetch(url);
        result = await result.json();


        let cours = result.map(el => {
            return {
                'rate': el.rate,
                'courency': el.cc,
            }
        }).filter(e => e.courency == 'USD' || e.courency == 'DKK' || e.courency == 'SEK' || e.courency == 'EUR' || e.courency == 'HUF' || e.courency == 'MXN' || e.courency == 'CHF');

        let tags = document.querySelectorAll('span');
        let re = /\d{1,}/g;
        let re1 = /[A-z]{3}/g;

        tags.forEach(el => {
            el.innerHTML = el.innerHTML.replace(re, function (price) {
                let found = el.innerHTML.match(re1).join('');
                price = el.innerHTML.match(re).join('');
                cours.forEach(element => {
                    if (element.courency == found) {
                        price = Math.round(price * element.rate)
                    }
                });
                return `${price} грн`;
            });
        });



        tags.forEach(item =>{
                    item.innerHTML = item.innerHTML.replace(re1, function(found){
                         found=found.replace(found,'');
                         return found;
                    });
        });



    })();
});